name = "katja"
zahl = 5
row_number = 43423523

Konstante = "unveränderbar"
#Konstante = "irgendwas"

p name
p zahl

#Sollen wir nach Hause gehen?
p "name is a #{name.class}"
p "zahl is a #{zahl.class}"

#p name + zahl
p name + zahl.to_s
p "#{name}#{zahl}"

p "name = #{name} und zahl = #{zahl}"
p "name = " + name + " und zahl = " + zahl.to_s

fliess_komma_zahl = 1.3
p "#{fliess_komma_zahl.class}"
p "#{fliess_komma_zahl.class}".class
p '#{fliess_komma_zahl}' #Funktioniert nicht mit singlequots
p fliess_komma_zahl.class

p :longus_schwanzus.class
puts :longus_schwanzus
